﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using T1_N00020449.Services;

namespace T1_N00020449
{
    public class Bowling
    {
        private readonly List<Player> ListJugador;
        private readonly List<Score> ListPuntaje;
        public Bowling()
        {
            ListJugador = new List<Player>();
            ListPuntaje = new List<Score>();
        }
        public void RegistrarJugador(Player jugador)
        {
            ListJugador.Add(jugador);
        }
        public List<Player> NumeroJugador()
        {
            return ListJugador;
        }
        public void RegistrarPuntaje(Score score)
        {
            ListPuntaje.Add(score);
        }

        
        public int GetScore()
        {
            int puntaje = 0;
            foreach (var player in ListJugador)
            {
                var lanzar = ListPuntaje.Where(o => o.IdJugador == player.Id).ToList();
                for (var move = 0; move < lanzar.Count(); move++)
                {
                    int tiro1 = lanzar[move].lanzar1;
                    int tiro2 = lanzar[move].lanzar2;
                    //puntaje = puntaje + tiro1 + tiro2;
                    if (tiro1 == 10 && lanzar.Count() - move > 1) //Puntaje Strike
                    {
                        if (lanzar[move + 1].lanzar1 == 10)
                        {
                            puntaje = puntaje + tiro1 + tiro2 + lanzar[move + 1].lanzar1; //+ lanzar[jugada + 1].lanzar2;
                        }
                        else
                        {
                            puntaje = puntaje + tiro1 + tiro2 + lanzar[move + 1].lanzar1 + lanzar[move + 1].lanzar2;
                        }
                    }
                    else if (tiro1 + tiro2 == 10 && lanzar.Count() - move > 1) //Puntaje Spare
                    {
                        puntaje = puntaje + tiro1 + tiro2 + lanzar[move + 1].lanzar1;
                    }
                    else //Puntaje Comun
                    {
                        puntaje = puntaje + tiro1 + tiro2;
                    }
                }
            }
            return puntaje;
        }
    }
}
