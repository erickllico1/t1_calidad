﻿using System;
using System.Collections.Generic;
using System.Text;

namespace T1_N00020449.Services
{
    public class Player
    {
        public int Id { get; set; }
        public String Nombre { get; set; }
        public List<Score> Punto { get; set; }
        public int Puntaje { get; set; }
    }
}
