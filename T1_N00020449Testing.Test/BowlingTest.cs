﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using T1_N00020449;
using T1_N00020449.Services;

namespace T1_N00020449Testing.Test
{
    [TestFixture]
    public class BowlingTest
    {
        //Pruebas Caso registrar jugadores
        [Test]
        public void Caso1Para1Jugadores()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Agapito" });
            var resultado = bolos.NumeroJugador().Count;//Para contar 
            Assert.AreEqual(1, resultado);
        }
        [Test]
        public void Caso2Para4Jugadores()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Agapito" });
            bolos.RegistrarJugador(new Player { Id = 2, Nombre = "Marcos" });
            bolos.RegistrarJugador(new Player { Id = 3, Nombre = "Moises" });
            bolos.RegistrarJugador(new Player { Id = 4, Nombre = "Luis" });
            var resultado = bolos.NumeroJugador().Count;
            Assert.AreEqual(4, resultado);//Para contar
        }
        [Test]
        public void Caso3Para0Jugadores()
        {
            var bolos = new Bowling();
            var resultado = bolos.NumeroJugador().Count;
            Assert.AreEqual(0, resultado);
        }

        // Pruebas Para lanzamientos
        [Test]
        public void Caso4Para0y5Puntos()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 0, lanzar2 = 5 });
            var resultado = bolos.GetScore();//resultado de puntaje
            Assert.AreEqual(5, resultado);
        }
        [Test]
        public void Caso5Para4y3puntos()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 4, lanzar2 = 3 });
            var resultado = bolos.GetScore();//resultado de puntaje
            Assert.AreEqual(7, resultado);
        }
        [Test]
        public void Caso6Para0y0puntos()
        {
            var bolos = new Bowling();
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 0, lanzar2 = 0 });
            var resultado = bolos.GetScore();//resultado de puntaje
            Assert.AreEqual(0, resultado);
        }
        [Test]
        public void Caso7Para5y5puntos()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 5, lanzar2 = 5 });
            var resultado = bolos.GetScore();//resultado de puntaje
            Assert.AreEqual(10, resultado);
        }
        [Test]
        public void Caso8Para0y10puntos()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 0, lanzar2 = 10 });
            var resultado = bolos.GetScore();//resultado de puntaje
            Assert.AreEqual(10, resultado);
        }
        [Test]
        public void Caso9Para10puntosStrike()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 });
            var resultado = bolos.GetScore();//resultado de puntaje
            Assert.AreEqual(10, resultado);
        }
        [Test]
        public void Caso10ParaPuntajeNoSpareNoStrike()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 7, lanzar2 = 1 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 0, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 3, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 0, lanzar2 = 0 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 8, lanzar2 = 1 });

            var resultado = bolos.GetScore();
            Assert.AreEqual(27, resultado);
        }
        [Test]
        public void Caso11ParaPuntajeSiSpareNoStrike()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 5, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 0, lanzar2 = 10 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 4, lanzar2 = 6 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 2, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });

            var resultado = bolos.GetScore();
            Assert.AreEqual(51, resultado);
        }
        [Test]
        public void Caso12ParaPuntajeNoSpareSiStrike()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 });
           // bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 3, lanzar2 = 6 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 });
           bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 });
            //bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 4, lanzar2 = 4 });

            var resultado = bolos.GetScore();
            Assert.AreEqual(50, resultado);
        }
        [Test]
        public void Caso13ParaPuntajeSiSpareNoStrikeFinal10()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 5, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 0, lanzar2 = 10 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 4, lanzar2 = 6 });
            
            var resultado = bolos.GetScore();
            Assert.AreEqual(34, resultado);
        }
        [Test]
        public void Caso14ParaPuntajeNoSpareSiStrikeFinal10()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 4, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });
            var resultado = bolos.GetScore();
            Assert.AreEqual(80, resultado);
        }
        [Test]
        public void Caso15ParaPuntajeNoSpareSiStrikeFinal10Y6y4()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 , lanzar2 = 0});//19
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 4, lanzar2 = 5 });//19+9=28
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10, lanzar2 = 0 });//28+10+next next ->6, 2 =46
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });//46 + 8 = 54
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10, lanzar2 = 0 });//54 + 10 +next next 6 ,4 =74
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 6, lanzar2 = 4 });//74 + 6+4=84
            var resultado = bolos.GetScore();
            Assert.AreEqual(84, resultado);
        }
        [Test]
        public void Caso16ParaPuntajeNoSpareSiStrikeFinal10Y10()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 4, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 });
            var resultado = bolos.GetScore();
            Assert.AreEqual(82, resultado);
        }
        [Test]
        public void Caso17ParaPuntajeSiSpareSiStrikeAlFinal()
        {
            var bolos = new Bowling();
            bolos.RegistrarJugador(new Player { Id = 1, Nombre = "Erick" });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 5, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 0, lanzar2 = 10 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 4, lanzar2 = 6 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 2, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 6, lanzar2 = 4 });
            bolos.RegistrarPuntaje(new Score { IdJugador = 1, lanzar1 = 10 });
            var resultado = bolos.GetScore();
            Assert.AreEqual(60, resultado);
        }
    }
}
